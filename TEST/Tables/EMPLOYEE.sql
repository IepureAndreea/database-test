CREATE TABLE "TEST".employee (
  cnp VARCHAR2(13 BYTE) NOT NULL,
  firstname VARCHAR2(20 BYTE),
  lastname VARCHAR2(20 BYTE),
  address VARCHAR2(20 BYTE),
  phonenumber VARCHAR2(10 BYTE),
  "PROJECT" VARCHAR2(20 BYTE),
  position VARCHAR2(10 BYTE),
  birthday DATE,
  CONSTRAINT employee_pk PRIMARY KEY (cnp)
);